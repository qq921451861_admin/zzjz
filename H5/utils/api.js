// get请求，获取小区
export const getCommunityList = (params = {}, config = {}) => uni.$uv.http.get('/communityList', {
	params: {
		pageNo: 1,
		pageSize: 999,
		...params
	},
	...config
})
// 
// get请求，获取列表
export const getList = (url = '', params = {}, config = {}) => uni.$uv.http.get(url, {
	params,
	...config
})


// get请求，获取通知消息列表
export const getNoticeList = (url = '', params = {}, config = {}) => uni.$uv.http.get(url, {
	params,
	...config
})


// post请求，保存客户留言
export const saveCustomerMsg = (params = {}, config = {}) => uni.$uv.http.post('/saveCustomerMsg', params, config)

//装修效果图-单个


// get请求，户型详情
export const getRoomPlan = (params = {}, config = {}) => uni.$uv.http.get('/getRoomPlan', {
	params,
	...config
})

// get请求，公告详情
export const getNotice = (params = {}, config = {}) => uni.$uv.http.get('/getNotice', {
	params,
	...config
})

// get请求，视频详情
export const getVideo = (params = {}, config = {}) => uni.$uv.http.get('/getVideo', {
	params,
	...config
})

// get请求，是否工作日
export const getWorkDay = (params = {}, config = {}) => uni.$uv.http.get('/getWorkDay', {
	params,
	...config
})


// get请求，获取初始化微信SDK的参数
export const getWechatJsConfig = (params = {}, config = {}) => uni.$uv.http.get('/getWechatJsConfig', {
	params,
	...config
})

