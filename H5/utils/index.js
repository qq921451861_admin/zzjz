export const jumpPlatform = ({
	type = 1, //1抖音，2快手
	videoId = ''
}) => {
	if (!videoId) {
		uni.showToast({
			title: '视频ID不能为空',
			icon: 'none'
		})
		return
	}
	const schemeLink = type == 1 ? 'snssdk1128://aweme/detail/' : 'kwai://work/';
	window.location.href = `${schemeLink}${videoId}`
}


export const isWx = () => {
	try {
		const ua = navigator.userAgent.toLowerCase();
		return ua.match(/MicroMessenger/i) == "micromessenger"
	} catch (e) {
		return false
	}
}

import {
	FILE_BASE_URL
} from '@/env.js'
export const getFileUlr = (src) => FILE_BASE_URL + src;

export const getAlbumUrl = (str) => {
	const arr = []
	if (str) {
		const sA = str.split(',')
		sA.forEach(v => {
			arr.push(getFileUlr(v))
		})
	}
	return arr
}