import {
	isWx
} from '@/utils/index.js'
import {
	getWechatJsConfig
} from '@/utils/api.js'

export const config = {
	debug: false,
	appId: 'wx3442cda1d586be94',
	jsApiList: ['closeWindow'],
	openTagList: ['wx-open-subscribe']
}

export const sdkInit = async () => {
	try {
		if (isWx()) {
			const wxInitSdkParm = await getWechatJsConfig({
				url: location.href.split('#')[0]
			});
			jWeixin.config({
				...config,
				signature:wxInitSdkParm.signature,
				nonceStr: wxInitSdkParm.nonceStr || wxInitSdkParm.noncestr,
				timestamp: wxInitSdkParm.timestamp,
			});
			jWeixin.ready((res) => {
				console.log('ready成功', res)
			})
			jWeixin.error((res) => {
				console.log('error', res)
			})
		}
	} catch (e) {
		//console.error(e)
		//TODO handle the exception
	}
}

export const propertyTmpId = 'IYFKNaLjF0zaQylbv8M4mpIHzi_yrTLSmXGXzrJ5s5E'; //物业发送公告的模版ID