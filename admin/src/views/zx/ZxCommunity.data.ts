import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { rules } from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '小区名称',
    align: "center",
    dataIndex: 'communityName'
  },
  {
    title: '图片',
    align: "center",
    dataIndex: 'communityPng',
    customRender: render.renderImage,
  },
  {
    title: '详细地址',
    align: "center",
    dataIndex: 'communityAddr'
  },
  {
    title: '房源形式',
    align: "center",
    dataIndex: 'roomType_dictText'
  },
  {
    title: '物业名称',
    align: "center",
    dataIndex: 'propertyName'
  },
  {
    title: '物业负责人',
    align: "center",
    dataIndex: 'propertyLeader'
  },
  {
    title: '物业联系方式',
    align: "center",
    dataIndex: 'propertyPhone'
  },
  {
    title: '排序',
    align: "center",
    dataIndex: 'sort'
  },
  {
    title: '是否启用',
    align: "center",
    dataIndex: 'enableStatus',
    customRender: ({ text }) => {
      return render.renderSwitch(text, [{ text: '是', value: 'Y' }, { text: '否', value: 'N' }])
    },
  },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: "小区名称",
    field: 'communityName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "物业名称",
    field: 'propertyName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "是否启用",
    field: 'enableStatus',
    component: 'JSwitch',
    componentProps: {
      query: true,
    },
    //colProps: {span: 6},
  },
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '小区名称',
    field: 'communityName',
    component: 'Input',
    dynamicRules: () => {
      return [
        { required: true, message: '请输入小区名称!' },
      ];
    },
  },
  {
    label: '图片',
    field: 'communityPng',
    component: 'JImageUpload',
  },
  {
    label: '详细地址',
    field: 'communityAddr',
    component: 'Input',
  },
  {
    label: '房源形式',
    field: 'roomType',
    component: 'JDictSelectTag',
    componentProps: {
      dictCode: "room_type"
    },
  },
  {
    label: '物业名称',
    field: 'propertyName',
    component: 'Input',
  },
  {
    label: '物业负责人',
    field: 'propertyLeader',
    component: 'Input',
  },
  {
    label: '物业联系方式',
    field: 'propertyPhone',
    component: 'Input',
  },
  {
    label: '排序',
    field: 'sort',
    component: 'InputNumber',
  },
  {
    label: '是否启用',
    field: 'enableStatus',
    defaultValue: "Y",
    component: 'JSwitch',
    componentProps: {
    },
    dynamicRules: () => {
      return [
        { required: true, message: '请输入是否启用!' },
      ];
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
  // TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false
  },
];

// 高级查询数据
export const superQuerySchema = {
  communityName: { title: '小区名称', order: 0, view: 'text', type: 'string', },
  communityPng: { title: '图片', order: 1, view: 'image', type: 'string', },
  communityAddr: { title: '详细地址', order: 2, view: 'text', type: 'string', },
  roomType: { title: '房源形式', order: 3, view: 'list', type: 'string', dictCode: 'room_type', },
  propertyName: { title: '物业名称', order: 4, view: 'text', type: 'string', },
  propertyLeader: { title: '物业负责人', order: 5, view: 'text', type: 'string', },
  propertyPhone: { title: '物业联系方式', order: 6, view: 'text', type: 'string', },
  sort: { title: '排序', order: 7, view: 'number', type: 'number', },
  enableStatus: { title: '是否启用', order: 8, view: 'switch', type: 'string', },
};

/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[] {
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}