import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
   {
    title: '姓名',
    align:"center",
    dataIndex: 'customerName'
   },
   {
    title: '联系方式',
    align:"center",
    dataIndex: 'customerPhone'
   },
   {
    title: '现住小区',
    align:"center",
    dataIndex: 'customerCommunity'
   },
   {
    title: '意向小区',
    align:"center",
    dataIndex: 'intentionCommunity'
   },
   {
    title: '户型',
    align:"center",
    dataIndex: 'roomType'
   },
   {
    title: '状态',
    align:"center",
    dataIndex: 'customerStatus_dictText'
   },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
	{
      label: "姓名",
      field: 'customerName',
      component: 'Input',
      //colProps: {span: 6},
 	},
	{
      label: "联系方式",
      field: 'customerPhone',
      component: 'Input',
      //colProps: {span: 6},
 	},
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '姓名',
    field: 'customerName',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入姓名!'},
          ];
     },
  },
  {
    label: '联系方式',
    field: 'customerPhone',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入联系方式!'},
                 { pattern: /^1[3456789]\d{9}$/, message: '请输入正确的手机号码!'},
          ];
     },
  },
  {
    label: '现住小区',
    field: 'customerCommunity',
    component: 'Input',
  },
  {
    label: '意向小区',
    field: 'intentionCommunity',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入意向小区!'},
          ];
     },
  },
  {
    label: '户型',
    field: 'roomType',
    component: 'Input',
  },
  {
    label: '状态',
    field: 'customerStatus',
    component: 'JDictSelectTag',
    componentProps:{
        dictCode:"customer_status"
     },
  },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];

// 高级查询数据
export const superQuerySchema = {
  customerName: {title: '姓名',order: 0,view: 'text', type: 'string',},
  customerPhone: {title: '联系方式',order: 1,view: 'text', type: 'string',},
  customerCommunity: {title: '现住小区',order: 2,view: 'text', type: 'string',},
  intentionCommunity: {title: '意向小区',order: 3,view: 'text', type: 'string',},
  roomType: {title: '户型',order: 4,view: 'text', type: 'string',},
  customerStatus: {title: '状态',order: 5,view: 'list', type: 'string',dictCode: 'customer_status',},
};

/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}