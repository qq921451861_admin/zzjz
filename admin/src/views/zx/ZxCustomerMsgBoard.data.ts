import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
   {
    title: '意向小区',
    align:"center",
    dataIndex: 'intentionCommunityId_dictText'
   },
   {
    title: '户型',
    align:"center",
    dataIndex: 'customerRoom'
   },
   {
    title: '姓名',
    align:"center",
    dataIndex: 'customerName'
   },
   {
    title: '联系方式',
    align:"center",
    dataIndex: 'customerPhone'
   },
   {
    title: '用户留言',
    align:"center",
    dataIndex: 'customerComment'
   },
   {
    title: '业务员名称',
    align:"center",
    dataIndex: 'salesName'
   },
   {
    title: '业务员电话',
    align:"center",
    dataIndex: 'salesPhone'
   },
   {
    title: '联系状态',
    align:"center",
    dataIndex: 'contactStatus_dictText'
   },
   {
    title: '联系结果',
    align:"center",
    dataIndex: 'contactResult'
   },
   {
    title: '排序',
    align:"center",
    dataIndex: 'sort'
   },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
	{
      label: "姓名",
      field: 'customerName',
      component: 'Input',
      //colProps: {span: 6},
 	},
	{
      label: "联系状态",
      field: 'contactStatus',
      component: 'JDictSelectTag',
      componentProps:{
          dictCode:"contact_status"
      },
      //colProps: {span: 6},
 	},
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '意向小区',
    field: 'intentionCommunityId',
    component: 'JDictSelectTag',
    componentProps:{
        dictCode:"zx_community,community_name,id"
     },
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入意向小区!'},
          ];
     },
  },
  {
    label: '户型',
    field: 'customerRoom',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入户型!'},
          ];
     },
  },
  {
    label: '姓名',
    field: 'customerName',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入姓名!'},
          ];
     },
  },
  {
    label: '联系方式',
    field: 'customerPhone',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入联系方式!'},
                 { pattern: /^1[3456789]\d{9}$/, message: '请输入正确的手机号码!'},
          ];
     },
  },
  {
    label: '用户留言',
    field: 'customerComment',
    component: 'InputTextArea',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入用户留言!'},
          ];
     },
  },
  {
    label: '业务员名称',
    field: 'salesName',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入业务员名称!'},
          ];
     },
  },
  {
    label: '业务员电话',
    field: 'salesPhone',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入业务员电话!'},
                 { pattern: /^1[3456789]\d{9}$/, message: '请输入正确的手机号码!'},
          ];
     },
  },
  {
    label: '联系状态',
    field: 'contactStatus',
    component: 'JDictSelectTag',
    componentProps:{
        dictCode:"contact_status"
     },
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入联系状态!'},
          ];
     },
  },
  {
    label: '联系结果',
    field: 'contactResult',
    component: 'InputTextArea',
  },
  {
    label: '排序',
    field: 'sort',
    component: 'InputNumber',
    dynamicRules: ({model,schema}) => {
          return [
                 { required: true, message: '请输入排序!'},
          ];
     },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
	// TODO 主键隐藏字段，目前写死为ID
	{
	  label: '',
	  field: 'id',
	  component: 'Input',
	  show: false
	},
];

// 高级查询数据
export const superQuerySchema = {
  intentionCommunityId: {title: '意向小区',order: 0,view: 'list', type: 'string',dictTable: "zx_community", dictCode: 'id', dictText: 'community_name',},
  customerRoom: {title: '户型',order: 1,view: 'text', type: 'string',},
  customerName: {title: '姓名',order: 2,view: 'text', type: 'string',},
  customerPhone: {title: '联系方式',order: 3,view: 'text', type: 'string',},
  customerComment: {title: '用户留言',order: 4,view: 'textarea', type: 'string',},
  salesName: {title: '业务员名称',order: 5,view: 'text', type: 'string',},
  salesPhone: {title: '业务员电话',order: 6,view: 'text', type: 'string',},
  contactStatus: {title: '联系状态',order: 7,view: 'list', type: 'string',dictCode: 'contact_status',},
  contactResult: {title: '联系结果',order: 8,view: 'textarea', type: 'string',},
  sort: {title: '排序',order: 9,view: 'number', type: 'number',},
};

/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[]{
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}