import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { rules } from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '发布方',
    align: "center",
    dataIndex: 'publishUser_dictText',
  },
  {
    title: '消息模版',
    align: "center",
    dataIndex: 'msgTemplateId_dictText',
  },
  {
    title: '标题',
    align: "center",
    dataIndex: 'noticeTitle'
  },
  {
    title: '公告时间',
    align: "center",
    dataIndex: 'msgTime'
  },
  {
    title: '发布状态',
    align: "center",
    dataIndex: 'publishStatus_dictText'
  },
  {
    title: '发布时间',
    align: "center",
    dataIndex: 'publishTime'
  },
  {
    title: '备注',
    align: "center",
    dataIndex: 'remark'
  },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: "标题",
    field: 'noticeTitle',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "发布状态",
    field: 'publishStatus',
    component: 'JDictSelectTag',
    componentProps: {
      dictCode: "publish_status"
    },
    //colProps: {span: 6},
  },
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '发布方',
    field: 'publishUser',
    component: 'JDictSelectTag',
    componentProps: {
      dictCode: "zx_community,community_name,id"
    },
    required: true,
  },
  {
    label: '消息模版',
    field: 'msgTemplateId',
    component: 'JDictSelectTag',
    componentProps: {
      dictCode: "message_template"
    },
    required: true,
  },
  {
    label: '标题',
    field: 'noticeTitle',
    component: 'Input',
    dynamicRules: () => {
      return [
        { required: true, message: '请输入标题!' },
      ];
    },
  },
  {
    label: '公告时间',
    field: 'msgTime',
    component: 'DatePicker',
    componentProps: {
      showTime: true,
      valueFormat: 'YYYY-MM-DD HH:mm:ss'
    },
  },
  {
    label: '内容',
    field: 'noticeContent',
    required: true,
    component: 'JEditor',
    componentProps: {
      showImageUpload: false,
      options: {
        fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 18pt 24pt 36pt",
        selector: 'textarea',
      },
      toolbar: 'fullscreen code preview | undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent lineheight|subscript superscript blockquote| numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons'
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'Input',
    componentProps: {
      allowClear: true,
      //是否展示字数
      showCount: true,
      //自适应内容高度，可设置为 true | false 或对象：{ minRows: 2, maxRows: 6 }
      autoSize: {
        //最小显示行数
        minRows: 2,
        //最大显示行数
        maxRows: 3
      },
    }
  },
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false
  },
];

// 高级查询数据
export const superQuerySchema = {
  noticeTitle: { title: '标题', order: 0, view: 'text', type: 'string', },
  noticeContent: { title: '内容', order: 1, view: 'textarea', type: 'string', },
  publishUser: { title: '发布方', order: 2, view: 'text', type: 'string', },
  publishStatus: { title: '发布状态', order: 3, view: 'list', type: 'string', dictCode: 'publish_status', },
  publishTime: { title: '发布时间', order: 4, view: 'datetime', type: 'string', },
};

/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[] {
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}