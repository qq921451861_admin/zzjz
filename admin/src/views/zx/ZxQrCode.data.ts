import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { rules } from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '名称',
    align: "center",
    dataIndex: 'codeName'
  },
  {
    title: '二维码链接',
    align: "center",
    dataIndex: 'codeUrl'
  },
  {
    title: '是否启用',
    align: "center",
    dataIndex: 'enableStatus',
    customRender: ({ text }) => {
      return render.renderSwitch(text, [{ text: '是', value: 'Y' }, { text: '否', value: 'N' }])
    },
  },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '名称',
    field: 'codeName',
    component: 'Input',
  },
  {
    label: '二维码链接',
    field: 'codeUrl',
    component: 'Input',
  },
  {
    label: '是否启用',
    defaultValue: "Y",
    field: 'enableStatus',
    component: 'JSwitch',
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
  // TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false
  },
];

// 高级查询数据
export const superQuerySchema = {
  codeName: { title: '名称', order: 0, view: 'text', type: 'string', },
  codeUrl: { title: '二维码链接', order: 1, view: 'text', type: 'string', },
  enableStatus: { title: '是否启用', order: 2, view: 'switch', type: 'string', },
};

/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[] {
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}