import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { rules } from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '所属小区',
    align: "center",
    dataIndex: 'communityId_dictText'
  },
  {
    title: '户型名称',
    align: "center",
    dataIndex: 'planName'
  },
  {
    title: '户形结构',
    align: "center",
    dataIndex: 'roomStructure_dictText'
  },
  {
    title: '户型图上传',
    align: "center",
    dataIndex: 'planUrl',
    customRender: render.renderImage,
  },
  {
    title: '户型效果图（外链）',
    align: "center",
    dataIndex: 'planLink'
  },
  {
    title: '设计师点评',
    align: "center",
    dataIndex: 'designerComment'
  },
  {
    title: '排序',
    align: "center",
    dataIndex: 'sort'
  },
  {
    title: '是否启用',
    align: "center",
    dataIndex: 'enableStatus',
    customRender: ({ text }) => {
      return render.renderSwitch(text, [{ text: '是', value: 'Y' }, { text: '否', value: 'N' }])
    },
  },
  {
    title: '户型面积',
    align: "center",
    dataIndex: 'roomArea'
  },
  {
    title: '推荐标签',
    align: "center",
    dataIndex: 'recommendTag_dictText'
  },
  {
    title: '更新日期',
    align: "center",
    dataIndex: 'updateTime'
  },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: "户型名称",
    field: 'planName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "是否启用",
    field: 'enableStatus',
    component: 'JSwitch',
    componentProps: {
      query: true,
    },
    //colProps: {span: 6},
  },
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '所属小区',
    field: 'communityId',
    component: 'JDictSelectTag',
    componentProps: {
      dictCode: "zx_community,community_name,id"
    },
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入所属小区!' },
      ];
    },
  },
  {
    label: '户型名称',
    field: 'planName',
    component: 'Input',
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入户型名称!' },
      ];
    },
  },
  {
    label: '户形结构',
    field: 'roomStructure',
    component: 'JDictSelectTag',
    componentProps: {
      dictCode: "room_structure"
    },
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入户形结构!' },
      ];
    },
  },
  {
    label: '户型面积',
    field: 'roomArea',
    component: 'Input',
  },
  {
    label: '户型图上传',
    field: 'planUrl',
    component: 'JImageUpload',
    componentProps: {
      fileMax: 9
    },
    required: true,
  },
  {
    label: '户型效果图（外链）',
    field: 'planLink',
    required: true,
    component: 'Input',
  },
  {
    label: '设计师点评',
    field: 'designerComment',
    component: 'InputTextArea',
  },
  {
    label: '排序',
    field: 'sort',
    component: 'InputNumber',
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入排序!' },
      ];
    },
  },
  {
    label: '是否启用',
    field: 'enableStatus',
    defaultValue: "Y",
    component: 'JSwitch',
    componentProps: {
    },
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入是否启用!' },
      ];
    },
  },
  {
    label: '推荐标签',
    field: 'recommendTag',
    component: 'JDictSelectTag',
    componentProps: {
      dictCode: "recommend_tag",
      maxTagCount:4,
      mode:"multiple"
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'Input',
  },
  // TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false
  },
];

// 高级查询数据
export const superQuerySchema = {
  communityId: { title: '所属小区', order: 0, view: 'list', type: 'string', dictTable: "zx_community", dictCode: 'id', dictText: 'community_name', },
  planName: { title: '户型名称', order: 1, view: 'text', type: 'string', },
  roomStructure: { title: '户形结构', order: 2, view: 'radio', type: 'string', },
  planUrl: { title: '户型图上传', order: 3, view: 'image', type: 'string', },
  planLink: { title: '户型效果图（外链）', order: 4, view: 'text', type: 'string', },
  designerComment: { title: '设计师点评', order: 5, view: 'textarea', type: 'string', },
  sort: { title: '排序', order: 6, view: 'number', type: 'number', },
  enableStatus: { title: '是否启用', order: 7, view: 'switch', type: 'string', },
  roomArea: { title: '户型面积', order: 8, view: 'text', type: 'string', },
  recommendTag: { title: '推荐标签', order: 9, view: 'list', type: 'string', dictCode: 'recommend_tag', },
  updateTime: { title: '更新日期', order: 11, view: 'datetime', type: 'string', },
};

/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[] {
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}