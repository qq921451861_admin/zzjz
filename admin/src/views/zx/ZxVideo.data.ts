import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { rules } from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '所属平台',
    align: "center",
    dataIndex: 'videoPlatform_dictText'
  },
  // {
  //   title: '视频账号ID',
  //   align: "center",
  //   dataIndex: 'accountId'
  // },
  {
    title: '视频ID',
    align: "center",
    dataIndex: 'videoId'
  },
  {
    title: '视频标题',
    align: "center",
    dataIndex: 'videoTitle'
  },
  {
    title: '视频封面',
    align: "center",
    dataIndex: 'coverUrl',
    customRender: render.renderImage,
  },
  {
    title: '排序',
    align: "center",
    dataIndex: 'sort'
  },
  {
    title: '是否启用',
    align: "center",
    dataIndex: 'enableStatus',
    customRender: ({ text }) => {
      return render.renderSwitch(text, [{ text: '是', value: 'Y' }, { text: '否', value: 'N' }])
    },
  },
];
//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: "所属平台",
    field: 'videoPlatform',
    component: 'JDictSelectTag',
    componentProps: {
      dictCode: "video_platform"
    },
    //colProps: {span: 6},
  },
  {
    label: "是否启用",
    field: 'enableStatus',
    component: 'JSwitch',
    componentProps: {
      query: true,
    },
    //colProps: {span: 6},
  },
];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '所属平台',
    field: 'videoPlatform',
    component: 'JDictSelectTag',
    componentProps: {
      dictCode: "video_platform"
    },
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入所属平台!' },
      ];
    },
  },
  // {
  //   label: '视频账号ID',
  //   field: 'accountId',
  //   component: 'Input',
  //   dynamicRules: ({ model, schema }) => {
  //     return [
  //       { required: true, message: '请输入视频账号ID!' },
  //     ];
  //   },
  // },
  {
    label: '视频ID',
    field: 'videoId',
    component: 'Input',
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入视频ID!' },
      ];
    },
  },
  {
    label: '视频标题',
    field: 'videoTitle',
    component: 'Input',
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入视频标题!' },
      ];
    },
  },
  {
    label: '视频封面',
    field: 'coverUrl',
    component: 'JImageUpload',
  },
  {
    label: '排序',
    field: 'sort',
    component: 'InputNumber',
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入排序!' },
      ];
    },
  },
  {
    label: '是否启用',
    field: 'enableStatus',
    defaultValue: "Y",
    component: 'JSwitch',
    componentProps: {
    },
    dynamicRules: ({ model, schema }) => {
      return [
        { required: true, message: '请输入是否启用!' },
      ];
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
  // TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false
  },
];

// 高级查询数据
export const superQuerySchema = {
  videoPlatform: { title: '所属平台', order: 0, view: 'list', type: 'string', dictCode: 'video_platform', },
  accountId: { title: '视频账号ID', order: 1, view: 'text', type: 'string', },
  videoId: { title: '视频ID', order: 2, view: 'text', type: 'string', },
  videoTitle: { title: '视频标题', order: 3, view: 'text', type: 'string', },
  sort: { title: '排序', order: 4, view: 'number', type: 'number', },
  enableStatus: { title: '是否启用', order: 5, view: 'switch', type: 'string', },
};

/**
* 流程表单调用这个方法获取formSchema
* @param param
*/
export function getBpmFormSchema(_formData): FormSchema[] {
  // 默认和原始表单保持一致 如果流程中配置了权限数据，这里需要单独处理formSchema
  return formSchema;
}